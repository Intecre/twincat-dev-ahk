# AutoHotKey for Beckhoff TwinCAT 3 Development helpers #

This autohot key script provides snippets that insert commonly used syntatical elements. 

### How do I get set up? ###

* I'm assuming you have [TC3](https://www.beckhoff.com/english.asp?twincat/twincat-3.htm) installed ;-)
* [Install auto hot keys](https://www.autohotkey.com)
* Clone this script to your local machine.
* Double click the script, or right-click "run ahk script".
* You can add this to your startup path if your want it to be always on.

### Contribution guidelines ###

* Please stick to the style of using a blank space following the combo to activate the entry 
* All PRs welcome.

### THANKS TO ###
Ben Harrison (beckhoff-benhar) - this idea was his, I've just extended it.