﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
#Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

:*:_ati ::AT %I* : ; typing "_ati " is replaced by input declaration
:*:_atq ::AT %Q* : ; typing "_atq " is replaced by output declaration
:*:_ass :::= ; typing "_ass " is replaced by the assignment operator
:*:_memset ::MEMSET( ADR( ), 0, SIZEOF( ) ); ; typing "_memset " is replaced by the memset snippet
:*:_memmove ::MEMMOVE( ADR( ), ADR( ), SIZEOF( ) ); ; typing "_ass " is replaced by memmove snippet
:*:_ifgate ::IF F_Gate(  ) THEN  ; typing "_ifgate " is replaced by if if_gate
:*:_attrCall ::{{}attribute 'monitoring' := 'call'{}} ; typing "_attrCall " is replace by attribute monitoring call 
:*:_attrVar ::{{}attribute 'monitoring' := 'variable'{}} ; typing "_attrVar " is replace by attribute monitoring variable 
:*:_attrAfterInit ::{{}attribute 'call_after_init'{}} ; typing "_attrVar " is replace by attribute monitoring variable 
:*:_isfalse :::= FALSE; ; typing "__isfalse " is replace by " := FALSE;" 
:*:_istrue :::= TRUE; ; typing "__istrue " is replace by " := TRUE;" 
